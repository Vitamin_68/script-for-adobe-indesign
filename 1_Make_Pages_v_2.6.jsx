#target "InDesign";
// Формат массива: 
// 0. Название устройства
// 1. Ширина пластины
// 2. Высота пластины
// 3. Клапан пластины (расстояние до печатного листа)
// 4. Максимальная ширина печатного листа
// 5. Максимальная высота печатного листа
const deviceData = new Array(["Ryoby", 510, 400, 20, 520, 365], ["Dominant", 650, 530, 21, 660, 485], ["Roland", 740, 605, 44, 740, 520], ["Принтер", 0, 0, 0, 1000, 330]);
const deviceName = 0;
const devicePlateWidth = 1;
const devicePlateHeight = 2;
const devicePlateKlapan = 3;
const deviceMaxPrintPageWidth = 4;
const deviceMaxPrintPageHeight = 5;

const myDevicePrinter = 3;
const cmykSquareSize = 5;
const pointValue = 2.834645;
const libraryMetkiName = 'Vision_metki.indl';
const newScriptPath = '//NAS1/Public/_soft/_Скрипт для сборок/';
const partScriptName = '1_Make_Pages_v_';

var topBorderPrintPage;
var leftBorderPrintPage;
var rightBorderPrintPage;
var bottomBorderPrintPage;
var bottomShift = 2;  			// default: сдвиг нижних крестов от края печатного листа вверх в мм

var printPageWidth;
var printPageHeight;
var printPageKlapan;
var plateWidth;
var plateHeight;

var myDocument;
var myPage;
var myDevice;
var myRegistrationColor;
var workLayer;
var serviceLayer;
var libraryMetki;
var flagRightPageSize = true;
var flagOverPageSize = false;

main();

function main() {
	// отключает перерисовку экрана во время выполнения скрипта
	app.scriptPreferences.enableRedraw = false;

	if (checkNewVersion()) {
    	return;
	}

	if (!haveLibrary()) {
		alert ('Для продолжения работы необходимо загрузить библиотеку меток ' + libraryMetkiName);
		return;
	}
	while (flagRightPageSize) {
		myDisplayDialog();
	}
}

function checkNewVersion() {
	var newScriptFolder = new Folder(newScriptPath);
	var arrFiles = newScriptFolder.getFiles();
	var myScript = app.activeScript;
	var regVersion = /\d+.\d+/;
	var currVerScript = regVersion.exec(myScript.name);
	var newVerScript;
    var myParentFolder = File (myScript) .parent;
	var regName = new RegExp(partScriptName);
	for (i=0; i < arrFiles.length; i++) {
		if (regName.test(arrFiles[i].name)) {
			newVerScript = regVersion.exec(arrFiles[i].name);
			if (newVerScript > currVerScript) {
				try { arrFiles[i].copy(myParentFolder + '/' + arrFiles[i].name);
					alert ('\tОбнаружена новая версия скрипта!\nФайл скопирован в папку скриптов и будет запущена\n\t\t   новая версия.\n\n\t   Старую версию можно удалить');
					var newScript = File(myParentFolder + '/' + arrFiles[i].name);
					app.doScript(newScript);
				} catch (e) {
					alert ('\tОшибка!\n\nНе удалось скопировать новую версию.');
				}
				return true;
			}
		}
	}
	return false;
}

function myDisplayDialog(){
    var myDialog = app.dialogs.add({name:"Выберите печатную машину:"});
	with(myDialog) {
    	with(dialogColumns.add()){
        	with(borderPanels.add()){
            	var myDeviceButtons = radiobuttonGroups.add();
                with(myDeviceButtons){
                	for (i = 0; i < deviceData.length - 1; i++) {
                		radiobuttonControls.add({staticLabel:"- " + deviceData[i][deviceName] + " " + deviceData[i][devicePlateWidth] + "x" + deviceData[i][devicePlateHeight] + " мм", checkedState: i==0});
                	}
                   	radiobuttonControls.add({staticLabel:"- Принтер"});
                }
            }
    		with(borderPanels.add()){
    			staticTexts.add({staticLabel:"Печатный лист:"});
    			with (dialogColumns.add()){
    				staticTexts.add({staticLabel:"Ширина:"});
    				staticTexts.add({staticLabel:"Высота:"});
    				staticTexts.add({staticLabel:"Клапан:"});
    				staticTexts.add({staticLabel:"Отступ от низа листа:"});
    			}
    			with (dialogColumns.add()){
    				printPageWidth = measurementEditboxes.add({editValue:(450 * pointValue), 
    					editUnits:MeasurementUnits.millimeters, minimumValue:200 * pointValue, maximumValue:1000 * pointValue, smallNudge:1});
    				printPageHeight = measurementEditboxes.add({editValue:(320 * pointValue), 
    					editUnits:MeasurementUnits.millimeters, minimumValue:150 * pointValue, maximumValue:520 * pointValue, smallNudge:1});
					printPageKlapan = measurementEditboxes.add({editValue:(12 * pointValue), 
						editUnits:MeasurementUnits.millimeters, minimumValue:8 * pointValue, maximumValue:50 * pointValue, smallNudge:1});
					bottomShift = measurementEditboxes.add({editValue:(bottomShift * pointValue), editUnits:MeasurementUnits.millimeters, smallNudge:1});
    			}
    		}  
    	}
	}

    var myReturn = myDialog.show();
    myDevice = myDeviceButtons.selectedButton;
    printPageWidth = Math.round(printPageWidth.editValue / pointValue);
    printPageHeight = Math.round(printPageHeight.editValue / pointValue);
    printPageKlapan = Math.round(printPageKlapan.editValue / pointValue);
    bottomShift = Math.round(bottomShift.editValue / pointValue);
    if (printPageWidth > deviceData[myDevice][deviceMaxPrintPageWidth] || printPageHeight > deviceData[myDevice][deviceMaxPrintPageHeight]) {
    	alert("    Недопустимые размеры печатного листа!\n\nМаксимальные размеры для " + deviceData[myDevice][deviceName] + " - " + 
    		deviceData[myDevice][deviceMaxPrintPageWidth] + 'x' + deviceData[myDevice][deviceMaxPrintPageHeight] + ' мм');
    	return;
    }
    if (myReturn == true) {
    	myDocument = app.documents.add();
		myPage = myDocument.pages.item(0);
		myRegistrationColor = myDocument.colors.item("Registration");

		with(myDocument.textDefaults){
			// alignToBaseline = true;
			//Because the font might not be available, it's usually best
			//to apply the font within a try...catch structure. Fill in the
			//name of a font on your system.
			try{
				appliedFont = app.fonts.item("Courier New");
			}
			catch(e){}
			//Because the font style might not be available, it's usually best
			//to apply the font style within a try...catch structure.
			try{
				fontStyle = "Bold";
			}
			catch(e){}
		}

    	// если выбран принтер: формат страницы индиза = печатному листу, margins = -3.5 мм с каждой стороны, это минимальные незапечатываемые поля
    	if (myDevice === myDevicePrinter) {
			with(myDocument.documentPreferences){
        		pageWidth = printPageWidth + "mm";
        		pageHeight = printPageHeight + "mm";
        		pageOrientation = PageOrientation.landscape;
    		}
    		with (myPage.marginPreferences) {
				columnCount = 2;
				top = 3.5;
				bottom = 3.5;
				left = 3.5;
				right = 3.5;
    		}
    	}
    	// если любая печатная машина:  формат страницы индиза = формату пластины, margins = печатному листу
    	else {
    		if (printPageWidth > deviceData[myDevice][devicePlateWidth]) {
    			alert ('\t\tВнимание!\n\nШирина бумаги больше ширины пластины, возможно некоторые метки необходимо будет сдвинуть в поле печати!');
    			flagOverPageSize = true;
    		}

    		plateWidth = deviceData[myDevice][devicePlateWidth];
    		plateHeight = deviceData[myDevice][devicePlateHeight];
			with (myDocument.documentPreferences){
	        	pageWidth = plateWidth + "mm";
	        	pageHeight = plateHeight + "mm";
	        	pageOrientation = PageOrientation.landscape;
	    	}
	        // границы печатного листа
	        topBorderPrintPage = deviceData[myDevice][devicePlateKlapan];
	        leftBorderPrintPage = (plateWidth - printPageWidth)/2;
	        rightBorderPrintPage = plateWidth - leftBorderPrintPage;
	        bottomBorderPrintPage = topBorderPrintPage + printPageHeight;
	    	
	    	with (myPage.marginPreferences) {
				top = topBorderPrintPage;
				left = flagOverPageSize ? 0 : leftBorderPrintPage;
				right = left;
				bottom = plateHeight - bottomBorderPrintPage;
				columnCount = 2;
				columnGutter = 0;        	
	    	}
	    	workLayer = myDocument.layers.item(0);
	    	workLayer.name = "Work";
	    	var pageLayer = myDocument.layers.add();
	    	pageLayer.name = "Page " + printPageWidth + "x" + printPageHeight + " mm";
	    	var pageBorderColor = myDocument.colors.add({name:"PageBorder", model:ColorModel.spot, colorValue:[0, 0, 0, 100]});
	    	myPage.rectangles.add({geometricBounds:[topBorderPrintPage, leftBorderPrintPage, bottomBorderPrintPage, rightBorderPrintPage], strokeColor:pageBorderColor, strokeWeight:1, overprintStroke:true});
	    	pageLayer.visible = false;
	    	pageLayer.locked = true;
	    	serviceLayer = myDocument.layers.add();
	    	serviceLayer.name = "Metki";
	  		serviceLayer.move (LocationOptions.after, app.documents.item(0).layers.item(-1));

	    	var myObj;
			
			// приводные кресты
			placeTargets(myObj);
			
			// клапан
			placeKlapan();
	    	
			// елка
			placeElka(myObj);
			
	  		// размер бумаги и пластины в клапане
	  		printInfo();

	  		myDocument.activeLayer = workLayer;
	  	}
		flagRightPageSize = false;
		app.activeWindow.zoom(ZoomOptions.FIT_PAGE);
	}
	else{
		myDialog.destroy();
		flagRightPageSize = false;
	}
}

function haveLibrary() {
	var result = false;
	for (i = 0; i < app.libraries.length; i++) {
		if (app.libraries.item(i).name == libraryMetkiName) {
			libraryMetki = app.libraries.item(i);
			result = true;
			break;
		}
	}
	return result;
}

function placeTargets(myObj) {
	var targetArray = new Array;
	myObj = libraryMetki.assets.itemByName('крест').placeAsset(myDocument)[0];
	var crossSize = myObj.geometricBounds[2] - myObj.geometricBounds[0];
	//верxний левый крест
	myObj.move([leftBorderPrintPage, topBorderPrintPage + printPageKlapan]);
	//верxний правый крест
	myObj.duplicate([rightBorderPrintPage - crossSize, topBorderPrintPage + printPageKlapan]);
	//нижний левый крест
	var myTarget = myObj.duplicate([leftBorderPrintPage, bottomBorderPrintPage - crossSize - bottomShift]);
	targetArray.push(myTarget);
	//нижний правый крест
	myTarget = myObj.duplicate([rightBorderPrintPage - crossSize, bottomBorderPrintPage - crossSize - bottomShift]);
	targetArray.push(myTarget);
	//нижний центральный крест
	var downWhiteSquare = myPage.rectangles.add({geometricBounds:[bottomBorderPrintPage - crossSize - bottomShift, (plateWidth - crossSize) / 2, 
		bottomBorderPrintPage - bottomShift, (plateWidth - crossSize) / 2 + crossSize], fillColor:myDocument.colors.item("Paper")});
	downWhiteSquare.sendToBack();
	targetArray.push(downWhiteSquare);
	myTarget = myObj.duplicate([(plateWidth - crossSize) / 2, bottomBorderPrintPage - crossSize - bottomShift]);
	targetArray.push(myTarget);
	myPage.groups.add(targetArray);
	// верхний центральный крест
	myObj = libraryMetki.assets.itemByName('крест центр').placeAsset(myDocument)[0];
	crossSize = myObj.geometricBounds[3] - myObj.geometricBounds[1];
	myObj.move ([(plateWidth - crossSize) / 2 , topBorderPrintPage + printPageKlapan - crossSize]);
}

function placeKlapan() {
	with(myPage) {
		var klapanGuide = guides.add(undefined, {orientation:HorizontalOrVertical.horizontal,
        	               location:(topBorderPrintPage + printPageKlapan)});
		klapanGuide.itemLayer = workLayer;
		klapanGuide.locked = true;
	}
	var myNoneSwatchColor = myDocument.swatches.item("None");
	var myStrokeWeight = 0.5;
	clapanLine = myPage.graphicLines.add({strokeWeight:myStrokeWeight, fillColor:myNoneSwatchColor, strokeColor:myRegistrationColor, 
		geometricBounds: [(topBorderPrintPage + printPageKlapan), (leftBorderPrintPage - 5), (topBorderPrintPage + printPageKlapan), (leftBorderPrintPage + 5)]});
	clapanLine.duplicate(undefined, [printPageWidth, 0]);
	// clapanLine.duplicate([rightBorderPrintPage - 5, topBorderPrintPage + printPageKlapan]);
	var textKlapan = myPage.textFrames.add();
	textKlapan.geometricBounds = [topBorderPrintPage + printPageKlapan - 5, leftBorderPrintPage + 2, topBorderPrintPage + printPageKlapan, leftBorderPrintPage + 33];
	textKlapan.contents = "Клапан " + printPageKlapan + " мм";
	var textKlapanObject = textKlapan.parentStory;
	textKlapanObject.fillColor = myRegistrationColor;
	textKlapan.duplicate([rightBorderPrintPage - 33, topBorderPrintPage + printPageKlapan - 5]);
}

function placeElka(myObj) {
	var elkaUpArray = new Array;
	var elkaDownArray = new Array;
	myObj = libraryMetki.assets.itemByName('Елка').placeAsset(myDocument)[0];
	var elkaHalfWidth = (myObj.geometricBounds[3] - myObj.geometricBounds[1]) / 2;
	myObj.move ([leftBorderPrintPage - elkaHalfWidth, topBorderPrintPage + printPageKlapan + 10]);
	elkaUpArray.push(myObj);
	elkaUpArray.push(myObj.duplicate([rightBorderPrintPage - elkaHalfWidth, topBorderPrintPage + printPageKlapan + 10]));
	elkaDownArray.push(myObj.duplicate([leftBorderPrintPage - elkaHalfWidth, bottomBorderPrintPage - 15 - bottomShift]));
	elkaDownArray.push(myObj.duplicate([rightBorderPrintPage - elkaHalfWidth, bottomBorderPrintPage - 15 - bottomShift]));
	myPage.groups.add(elkaUpArray);
	myPage.groups.add(elkaDownArray);
}

function printInfo() {
	var infoList = myPage.textFrames.add();
	infoList.geometricBounds = [topBorderPrintPage + printPageKlapan - 10, leftBorderPrintPage + 2, topBorderPrintPage + printPageKlapan - 5, leftBorderPrintPage + 192];
	// t.parentStory.texts.item(0).fontStyle = "Bold";
	infoList.contents = "Сборка на " + deviceData[myDevice][deviceName] + 
		"   Печатный лист - " + printPageWidth + "х" + printPageHeight + " мм   " + "Пластина - " + plateWidth + "х" + plateHeight + " мм";
	var infoListObject = infoList.parentStory;
	infoListObject.fillColor = myRegistrationColor;
	var infoData = myPage.textFrames.add();
	infoData.geometricBounds = [topBorderPrintPage + printPageKlapan - 10, leftBorderPrintPage + 200, topBorderPrintPage + printPageKlapan - 5, leftBorderPrintPage + 265];
	infoData.contents = Date();
	var infoDataObject = infoData.parentStory;
	infoDataObject.fillColor = myRegistrationColor;
	var id = infoData.createOutlines();
	id[0].label = "CreateDate";

	// infoDataObject.appliedLanguage = app.languagesWithVendors.item(44);  // язык не работает((
	// infoList.parentStory.texts.item(0).fontStyle = "Bold";
	// infoList.parentStory.texts.item(0)[1].fontStyle = "Bold";
}	