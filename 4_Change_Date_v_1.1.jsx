// Скрипт меняет старую дату в сборке (в клапане) на текущую.
// С 23.03.2021 при генерации листа на дату устанавливается метка "CreateDate"
// и скрипт поменяет ее автоматически. Если сборка делалась раньше, то этой метки не будет
// и дату для замены нужно будет выделить мышкой.

var myDocument = app.documents.item(0);
var myPage = app.activeWindow.activePage;
var myRegistrationColor = myDocument.colors.item("Registration");
var indexDate = -1;

const newScriptPath = "//NAS1/Public/_soft/_Скрипт для сборок/";
const partScriptName = "4_Change Date_v_";
const myScript = app.activeScript;

// после выполнения скрипта его можно будет отменить одним кликом - CTRL+Z
app.doScript(main, ScriptLanguage.javascript, undefined, UndoModes.entireScript, "Замена даты");

function main() {
    if (checkNewVersion()) {
        return;
    }

    if (app.documents.length == 0) {
        alert ('Нет открытого документа!');
        return;
    }

    with (myDocument.textDefaults){
        try{
            appliedFont = app.fonts.item("Courier New");
        }
        catch(e){
            alert ("Шрифт Courier New не установлен");
        }
        try{
            fontStyle = "Bold";
        }
        catch(e){
            alert ("Шрифт Courier New Bold не установлен");
        }
    }

    for (var i = 0; i < myPage.pageItems.length; i++)
    {
        if (myPage.pageItems.item(i).label == "CreateDate")
        {
            indexDate = i;
            break;
        }
    }
    if (indexDate >= 0) 
    {
        replaceDate(indexDate);
    }
    else
    {
        selElements = (app.selection.length < 2) ? app.selection.length : 2;
        switch (selElements)
        {
            case 0:
                alert ("Метку даты не найдено,\nвыделите дату мышкой и перезапустите скрипт.");
                break;
            case 1:
                var myResult = confirm ("Вы действительно хотите заменить этот элемент на текущую дату?");
                if (myResult) {
                    replaceDate(indexDate);
                }
                else{
                    alert ("Действие отменено.");
                }
                break;
            case 2:
                alert ("Выбрано больше 1 элемента!")
                break;
        }
    }
}

function replaceDate(i) {
    var topMargin, leftMargin;
    if (i >= 0) {
        topMargin = myPage.pageItems.item(i).geometricBounds[0] + 0.06;
        leftMargin = myPage.pageItems.item(i).geometricBounds[1] - 0.17;
        myPage.pageItems.item(i).remove();
    }
    else {
        topMargin = myDocument.selection[0].geometricBounds[0] + 0.06;
        leftMargin = myDocument.selection[0].geometricBounds[1] - 0.17;
        myDocument.selection[0].remove();
    }
    var infoDate = myPage.textFrames.add();
    infoDate.geometricBounds = [topMargin, leftMargin, topMargin + 5, leftMargin + 65];
    infoDate.contents = Date();
    var infoDateObject = infoDate.parentStory;
    infoDateObject.fillColor = myRegistrationColor;
    var id = infoDate.createOutlines();
    id[0].label = "CreateDate";
    alert ("Дата успешно изменена.");
}

function checkNewVersion() {
    var newScriptFolder = new Folder(newScriptPath);
    var arrFiles = newScriptFolder.getFiles();
    var regVersion = /\d+.\d+/;
    var currVerScript = regVersion.exec(myScript.name);
    var newVerScript;
    var myParentFolder = File (myScript) .parent;
    var regName = new RegExp(partScriptName);
    for (i=0; i < arrFiles.length; i++) {
        if (regName.test(arrFiles[i].name)) {
            newVerScript = regVersion.exec(arrFiles[i].name);
            if (newVerScript > currVerScript) {
                try { arrFiles[i].copy(myParentFolder + '/' + arrFiles[i].name);
                    alert ('\tОбнаружена новая версия скрипта!\nФайл скопирован в папку скриптов и будет запущена\n\t\t   новая версия.\n\n\t   Старую версию можно удалить');
                    var newScript = File(myParentFolder + '/' + arrFiles[i].name);
                    app.doScript(newScript);
                } catch (e) {
                    alert ('\tОшибка!\n\nНе удалось скопировать новую версию.');
                }
                return true;
            }
        }
    }
    return false;
}
    
