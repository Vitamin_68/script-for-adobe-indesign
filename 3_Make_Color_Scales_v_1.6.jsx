// Скрипт 

var myDocument;
var myPage;
var mySwatchNamesList = new Array('Cyan', 'Magenta', 'Yellow', 'Black'); // в начале списка будет CMYK
var mySwatchNamesListChecked = new Array;
var mySwatchNamesListExclude = new Array('Cyan', 'Magenta', 'Yellow', 'Black', '', 'kroj', 'cut', 'registration', 'paper', 'nog', 'PageBorder');
var counterLayers;
var libraryMetki;
var bottomShift = 2;     // default: сдвиг шкалок от нижнего края печатного листа вверх в мм


const pointValue = 2.834645;
const scaleSquareSize = 5;
const registrationCrossSize = 5;
const libraryMetkiName = 'Vision_metki.indl';
const plateHeight;
const plateWidth;
const topBorderPrintPage;
const bottomBorderPrintPage;
const leftBorderPrintPage;
const oneCharLenght = 2.55;
const newScriptPath = '//NAS1/Public/_soft/_Скрипт для сборок/';
const partScriptName = '3_Make_Color_Scales_v_';

main();

function main(){
	if (app.documents.length == 0) {
		alert ('Нет открытого документа!');
		return;
	}
	myDocument = app.activeDocument;
	myPage = app.activeWindow.activePage;
	plateHeight = myDocument.documentPreferences.pageHeight;
	plateWidth = myDocument.documentPreferences.pageWidth;
	topBorderPrintPage = myPage.marginPreferences.top;
	bottomBorderPrintPage = myDocument.documentPreferences.pageHeight - myPage.marginPreferences.bottom;
	leftBorderPrintPage = myPage.marginPreferences.left;
	// отключает перерисовку экрана во время выполнения скрипта
	app.scriptPreferences.enableRedraw = false;
	if (checkNewVersion()) {
    	return;
	}
	if (!isHasLibrary()) {
		alert ('Для продолжения работы необходимо загрузить библиотеку меток ' + libraryMetkiName);
		return;
	}
	deleteUnusedSwatch();
	for(var myCounter = 0; myCounter < myDocument.colors.length; myCounter ++){
		//Do not add() unnamed colors.
		var colorName = myDocument.colors.item(myCounter).name;
		var colorRegEx = /^c\d+m/;
		if(!excludeSwatches(colorName) && !colorRegEx.test(colorName)) {
			mySwatchNamesList.push(myDocument.colors.item(myCounter).name);
		}
	}
	var myDialog = app.dialogs.add({name:"Создание шкалок на лист"});
	var mylakList = new Array();
	mylakList.push("Выберите из списка");
	with (myDialog) {
		with(dialogColumns.add()){
			with(borderPanels.add()){
				staticTexts.add({staticLabel:"Выберите краски для шкалы:"});
				with(dialogColumns.add()){		
					for (i=0; i<mySwatchNamesList.length; i++){			
						tmpName = mySwatchNamesList[i];
						mylakList.push(mySwatchNamesList[i]);
						mySwatchNamesList[i] = new Array(2);
						mySwatchNamesList[i][0] = tmpName;
						mySwatchNamesList[i][1] = checkboxControls.add({staticLabel:tmpName, checkedState: checkLak(tmpName)});
					}
				}
			}
			with(borderPanels.add()){
				with(dialogColumns.add()){
					staticTexts.add({staticLabel:"Отступ шкалы от низа листа:"});
				}
				with (dialogColumns.add()){
    				bottomShift = measurementEditboxes.add({editValue:(bottomShift * pointValue), editUnits:MeasurementUnits.millimeters, smallNudge:1});
    			}
			}
			with(borderPanels.add()){
                with(dialogColumns.add()){
                    staticTexts.add({staticLabel:"Лак:"});
                }   
                with(dialogColumns.add()){
                    //Create a pop-up menu ("dropdown") control.
                    var myLakIndex = dropdowns.add({stringList:mylakList, selectedIndex:0});
                }
            }
		}
	}
	var myResult = myDialog.show();
	bottomShift = Math.round(bottomShift.editValue / pointValue);
 	if (myResult) {
		// создание массива отмеченных красок
		for (i = 0; i < mySwatchNamesList.length; i++) {
			if (mySwatchNamesList[i][1].checkedState == true) {
				mySwatchNamesListChecked.push(mySwatchNamesList[i]);
			}
		}
		if (mySwatchNamesListChecked.length > 0) {
			var myLayerName = namingLayer('Color_scales');
			var colorLayer = myDocument.layers.add({name: myLayerName});
		  	colorLayer.move (LocationOptions.after, app.documents.item(0).layers.item(-1));

			// шкалки растискивания
  			placeSuns(libraryMetki);

			// раскатные шкалки
			placeScales();

			if (mySwatchNamesListChecked.length > 1) {
				//шкала треппинга
				placeScalaTrepp();
			}

			// подписи плоскостей
			var myLakSwatch = "";
			if (myLakIndex.selectedIndex > 0) {
				myLakSwatch = mylakList[myLakIndex.selectedIndex];
			}
			var posY = placeNameSwatches(myLakSwatch);

			//имя файла
		  	printFileName(posY);
		} else {
			alert ('Нет выделенных элементов');
		}
	}
}

function checkNewVersion() {
	var newScriptFolder = new Folder(newScriptPath);
	var arrFiles = newScriptFolder.getFiles();
	var myScript = app.activeScript;
	var regVersion = /\d+.\d+/;
	var currVerScript = regVersion.exec(myScript.name);
	var newVerScript;
    var myParentFolder = File (myScript) .parent;
	var regName = new RegExp(partScriptName);
	for (i=0; i < arrFiles.length; i++) {
		if (regName.test(arrFiles[i].name)) {
			newVerScript = regVersion.exec(arrFiles[i].name);
			if (newVerScript > currVerScript) {
				try { arrFiles[i].copy(myParentFolder + '/' + arrFiles[i].name);
					alert ('\tОбнаружена новая версия скрипта!\nФайл скопирован в папку скриптов и будет запущена\n\t\t   новая версия.\n\n\t   Старую версию можно удалить');
					var newScript = File(myParentFolder + '/' + arrFiles[i].name);
					app.doScript(newScript);
				} catch (e) {
					alert ('\tОшибка!\n\nНе удалось скопировать новую версию.');
				}
				return true;
			}
		}
	}
	return false;
}

function isHasLibrary() {
	var result = false;
	for (i = 0; i < app.libraries.length; i++) {
		if (app.libraries.item(i).name == libraryMetkiName) {
			libraryMetki = app.libraries.item(i);
			result = true;
			break;
		}
	}
	return result;
}

function deleteUnusedSwatch(){
	var id, sw;
	while (myDocument.unusedSwatches[0].name != "") {
		id = myDocument.unusedSwatches[0].id;
		sw = myDocument.swatches.itemByID(id);
		sw.remove();
	}
}

function excludeSwatches(colorName) {
	var colorExclude = false;
	for (i = 0; i < mySwatchNamesListExclude.length; i++) {
		if (colorName.toLowerCase() == mySwatchNamesListExclude[i].toLowerCase()) {
			colorExclude = true;
		}
	}
	return colorExclude;
}

function placeSuns(libraryMetki) {
    var sunArrayLeft = new Array;
    var sunArrayLeftRight = new Array;
   	var sunGroup = new Array;
   	var topObj = bottomBorderPrintPage - 25;
   	var myObj = libraryObject('Sun');
	if (myObj != undefined) {
	   	for (i = 0; i < mySwatchNamesListChecked.length; i++){
			myObj.move([myPage.marginPreferences.left, topObj]);
			topObj -= scaleSquareSize;
			myObj.fillColor = mySwatchNamesListChecked[i][0];
			sunArrayLeft.push(myObj);
			myObj = myObj.duplicate(undefined, [plateWidth - (myPage.marginPreferences.left + myPage.marginPreferences.right) - scaleSquareSize, 0]);
			sunArrayLeftRight.push(myObj);
			myObj = libraryObject('Sun');
	   	}
	   	myObj.remove();
   	// группировка сначала слева, затем справа и все вместе. Ругалось при попытке сгруппировать 1 объект.
	if (sunArrayLeft.length > 1) {
		sunGroup.push(myPage.groups.add(sunArrayLeft));
		sunGroup.push(myPage.groups.add(sunArrayLeftRight));
	} else {
		sunGroup.push(sunArrayLeft[0]);
		sunGroup.push(sunArrayLeftRight[0]);
	}
	myPage.groups.add(sunGroup);
	}
}

function placeScales() {
	var printPageWidth = plateWidth - myPage.marginPreferences.left * 2;
	startPosLeftX = myPage.marginPreferences.left + scaleSquareSize + (((printPageWidth - scaleSquareSize) / 2) % scaleSquareSize);
	startPosY = bottomBorderPrintPage - registrationCrossSize - bottomShift;
	quantitySquares =(plateWidth - startPosLeftX * 2) / scaleSquareSize;
	drawColorSquares(startPosLeftX, startPosY, quantitySquares);
}

function drawColorSquares(startX, startY, quantity) {
	var colorSquare;
	var squaresArray = new Array;
	var myGroup;
	for (i = 0; i < quantity; i += 2) {
		for (j = 0; j < 2; j++){
			if ((i + j) < quantity) {
				colorSquare = myPage.rectangles.add({fillColor:myDocument.colors.item(mySwatchNamesListChecked[(i/2)%mySwatchNamesListChecked.length][0]), strokeColor: 'None', fillTint:(100/(j+1)), 
					geometricBounds:[startY, startX + scaleSquareSize * (i+j), startY + scaleSquareSize, startX + scaleSquareSize * (i+j) + scaleSquareSize]});
				squaresArray.push(colorSquare);
			}
		}
	}
	//Group the blend items.
	myGroup = myPage.groups.add(squaresArray);
	//Assign the group to a layer.
	// myGroup.itemLayer = serviceLayer;
}

function namingLayer(nameIn, counterL) {
	if (counterL === undefined) {
		counterL = 0;
	}
	counterLayer = counterL;
	var name;
	if (counterLayer == 0) {
		name = nameIn;
	} else {
		name = nameIn + counterLayer;
	}
	for (i = 0; i < app.activeDocument.layers.length; i++) {
		if (app.activeDocument.layers.item(i).name == name) {
			namingLayer(nameIn, ++counterLayer);
			break;
		}
	}
	if (counterLayer == 0) {
		return nameIn;
	} 
	else {
		return nameIn + counterLayer;
	}
}

function placeScalaTrepp() {
	var myObj = libraryObject('Shkala_Trapp');
	var scalaTrappLength = myObj.geometricBounds[3] - myObj.geometricBounds[1];
	if (myObj != undefined) {
		myObj.move ([(plateWidth - scalaTrappLength) / 2, bottomBorderPrintPage - scaleSquareSize - bottomShift]);
	}
}

function libraryObject(nameObject) {
	try {
		return libraryMetki.assets.itemByName(nameObject).placeAsset(myDocument)[0];
	}
	catch(e) {
		alert ('Объект \"' + nameObject + '\" не найден в библиотеке \"' + libraryMetki.name + '\" и не был помещен на лист.');
	}
}

function placeNameSwatches(myLakSwatch) {
	var nameSwatch, nameSwatchObject, myGroup;
	var swatchArray = new Array;
	var myNamedSwatchArray = new Array;
	var charCount = 0;
	var nameLenght = 0;
	// массив имен
	for (i = 0; i < mySwatchNamesListChecked.length; i++){
		myNamedSwatchArray.push(mySwatchNamesListChecked[i][0]);
	}
	// если выбран лак - добавляем в массив имен
	if (myLakSwatch != "") {
		myNamedSwatchArray.push(myLakSwatch);
	}
	for (i = 0; i < myNamedSwatchArray.length; i++){
		nameLenght = myNamedSwatchArray[i].length + 1;
		nameSwatch = myPage.textFrames.add();
		nameSwatch.geometricBounds = [100, leftBorderPrintPage + charCount * oneCharLenght, 104, leftBorderPrintPage + (charCount + nameLenght) * oneCharLenght];
		nameSwatch.contents = myNamedSwatchArray[i];
		nameSwatchObject = nameSwatch.parentStory;
		nameSwatchObject.fillColor = myDocument.colors.item(myNamedSwatchArray[i]);
		charCount += nameLenght;
		swatchArray.push(nameSwatch);
	}
	if (swatchArray.length > 1) {
	myGroup = myPage.groups.add(swatchArray);
	} else {
		myGroup = nameSwatch;
	}
	var myRotateMatrix = app.transformationMatrices.add({counterclockwiseRotationAngle:90});
	myGroup.transform(CoordinateSpaces.pasteboardCoordinates, AnchorPoint.centerAnchor, myRotateMatrix);
	var bottomCoordinates = topBorderPrintPage + 30 + charCount * oneCharLenght;
	myGroup.move([leftBorderPrintPage, bottomCoordinates]);
	return bottomCoordinates;
}

function printFileName(posY) {
	var myObj = libraryObject('FileName');
	if (myObj != undefined) {
		myObj.move ([leftBorderPrintPage, posY + 90]);
	}
}

function checkLak(checkName) {
	var regLak = new RegExp("lak", "i");
	if (regLak.test(checkName)) {
		return false;
	}
	return true;
}