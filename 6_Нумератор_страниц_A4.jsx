// скрипт предназначен ТОЛЬКО! для шаблона многостраничных буклетов формата А4 с чужим оборотом, 
// он расставляет на страницах номера листов в виде 1-А, 1-В, 2-А, 2-В и т.д. на левой стороне сборки в слое "Work"
// в слое "PDF page order" расставляет порядок вставки страниц из многостраничного файла PDF
// сам шаблон лежит на NAS1 - \\NAS1\Public\_Шаблоны\Шаблон_многостр каталог_A4.indd

var myDocument = app.activeDocument;
// отключаем перерисовку экрана во время выполнения скрипта
app.scriptPreferences.enableRedraw = false;
// после выполнения скрипта его можно будет отменить одним кликом - CTRL+Z
app.doScript(main, ScriptLanguage.javascript, undefined, UndoModes.entireScript, "Нумератор страниц");

// main();

function main() {
    var pageCount = 1;
    try {
        var pageOrderColor = myDocument.colors.add({name:"PageOrder", model:ColorModel.spot, colorValue:[0, 100, 100, 40]});
    } catch (e) {}
    var pageOrderArray = new Array([1, 3], [4, 2]);
    var pageName, pageOrder;
    var myRotateMatrix = app.transformationMatrices.add({counterclockwiseRotationAngle:90});
    var myfacingPages = myDocument.documentPreferences.facingPages;
    myDocument.documentPreferences.facingPages = false;
    var workLayer = myDocument.layers.item("Work");
    var colorLayer = myDocument.layers.add();
    colorLayer.name = namingLayer('PDF page order');
    colorLayer.move (LocationOptions.after, app.documents.item(0).layers.item(-1));
    for (var i = 0; i < myDocument.pages.length; i++) {
        if (i % 2 == 0) {
            pageSymb = 'A';
        } else {
            pageSymb = 'B';
        }
        pageName = myDocument.pages.item(i).textFrames.add();
        pageName.geometricBounds = [280, 52, 285, 75];
        pageName.move(workLayer);
        pageName.contents = pageCount + "-" + pageSymb;
        pageName.parentStory.fillColor = myDocument.colors.item("Registration");
        pageName.parentStory.pointSize = 20;
        pageName.transform(CoordinateSpaces.pasteboardCoordinates, AnchorPoint.TOP_LEFT_ANCHOR, myRotateMatrix);
        pageCount += i % 2;
        for (var j = 0; j<2; j++) {
            pageOrderTxt = myDocument.pages.item(i).textFrames.add();
            pageOrderTxt.geometricBounds = [190, 50 + 320*j, 340, 370 + 320*j];
            pageOrderTxt.contents = (pageOrderArray[i%2][j] + (i-i%2)*2).toString();
            pageOrderTxt.parentStory.fillColor = myDocument.colors.item("PageOrder");
            pageOrderTxt.parentStory.justification = Justification.centerAlign;
            pageOrderTxt.parentStory.pointSize = 500;
        }
    }
    colorLayer.locked = true;
    myDocument.documentPreferences.facingPages = myfacingPages;
    myDocument.activeLayer = workLayer;
}

function namingLayer(nameIn, counterL) {
    if (counterL === undefined) {
        counterL = 0;
    }
    counterLayer = counterL;
    var name;
    if (counterLayer == 0) {
        name = nameIn;
    } else {
        name = nameIn + counterLayer;
    }
    for (i = 0; i < app.activeDocument.layers.length; i++) {
        if (app.activeDocument.layers.item(i).name == name) {
            namingLayer(nameIn, ++counterLayer);
            break;
        }
    }
    if (counterLayer == 0) {
        return nameIn;
    } 
    else {
        return nameIn + counterLayer;
    }
}