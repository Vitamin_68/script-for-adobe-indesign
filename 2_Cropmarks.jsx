 //CropMarks.jsx
//An InDesign CS3 JavaScript
/*  
@@@BUILDINFO@@@ "CropMarks.jsx" 1.0.0 14-September-2006
*/
//Draws crop and/or registration marks around the selected object or objects.
//
//For more on InDesign scripting, go to http://www.adobe.com/products/indesign/scripting.html
//or visit the InDesign Scripting User to User forum at http://www.adobeforums.com
//
//
// --------- Modified by Wetal ---------
// - добавил проверку названия слоев, если такой слой уже есть, то добавит новый с индексом 1, 2, 3...
// - добавил заливку выделенных объектов белым - в некоторых случаях, например в ярлыках или этикетках,
// может отсутствовать белая подложка, а ярлыки могут заходить на метки. В таком случае метка напечатается на ярлыке.
// - добавил выбор цвета для меток из выпадающего меню, по умолчанию Registration/
//

main();
function main(){
	if (app.documents.length != 0){
		if (app.selection.length > 0){
			switch(app.selection[0].constructor.name){
				case "Rectangle":
				case "Oval":
				case "Polygon":
				case "GraphicLine":
				case "Group":
				case "TextFrame":
				case "Button":
					myDisplayDialog();
					break;
				default:			
					alert("Ни один объект не выбран!");
					break;
			}
		}
		else{
			alert("Ничего не выделено");
		}
	}
	else{
		alert("Документ не открыт");
	}
}

function myDisplayDialog(){
	var mySwatchNamesList = new Array("Registration", "Cyan", "Magenta", "Yellow", "Black");
	var myDialog = app.dialogs.add({name:"Cropmarks"});

	deleteUnusedSwatch();
	for(var myCounter = 0; myCounter < app.activeDocument.colors.length; myCounter ++){
		//Do not add() unnamed colors.
		var colorName = app.activeDocument.colors.item(myCounter).name;
		var colorRegEx = /^c\d+m/;
		if(!excludeSwatches(colorName) && !colorRegEx.test(colorName)) {
			mySwatchNamesList.push(app.activeDocument.colors.item(myCounter).name);
		}
	}
	with(myDialog){
		with(dialogColumns.add()){
			var myCropMarksGroup = enablingGroups.add({staticLabel:"Метки реза", checkedState:true});
			with (myCropMarksGroup){
				with(borderPanels.add()){
					staticTexts.add({staticLabel:"Опции:"});
					with (dialogColumns.add()){
						staticTexts.add({staticLabel:"Длина:"});
						staticTexts.add({staticLabel:"Расстояние от объекта:"});
						staticTexts.add({staticLabel:"Обрезка объекта (ов):"});
						staticTexts.add({staticLabel:"Толщина линий:"});
						staticTexts.add({staticLabel:"Цвет меток реза:"});
						staticTexts.add({staticLabel:"Белая подложка под объектами"});
					}
					with (dialogColumns.add()){
						var myCropMarkLengthField = measurementEditboxes.add({editValue:(2*2.83465), editUnits:MeasurementUnits.millimeters, smallNudge:0.5});
						var myCropMarkOffsetField = measurementEditboxes.add({editValue:(0*2.83465), editUnits:MeasurementUnits.millimeters, smallNudge:0.5});
						var myCropMarkBleedField = measurementEditboxes.add({editValue:(1*2.83465), editUnits:MeasurementUnits.millimeters, smallNudge:0.5});
						var myCropMarkWidthField = measurementEditboxes.add({editValue:.5, editUnits:MeasurementUnits.points, smallNudge:0.1});
						var myLabelIndex = dropdowns.add({stringList:mySwatchNamesList, selectedIndex:0});
						// var myWhiteBox = checkboxControls.add({staticLabel:"Белая подложка под выделенными объектами", checkedState: true});
						var myWhiteBox = checkboxControls.add({checkedState: true});
					}
				}
			}
			var myRegMarksGroup = enablingGroups.add({staticLabel:"Метки приводки", checkedState:false});
			with (myRegMarksGroup){
				with(borderPanels.add()){
					staticTexts.add({staticLabel:"Опции:"});
					with (dialogColumns.add()){
						staticTexts.add({staticLabel:"Внутренний радиус:"});
						staticTexts.add({staticLabel:"Внешний радиус:"});
						staticTexts.add({staticLabel:"Расстояние:"});
					}
					with (dialogColumns.add()){
						var myRegMarkInnerRadiusField = measurementEditboxes.add({editValue:(1*2.83465), editUnits:MeasurementUnits.millimeters, smallNudge:0.5});
						var myRegMarkOuterRadiusField = measurementEditboxes.add({editValue:(2*2.83465),editUnits:MeasurementUnits.millimeters, smallNudge:0.5});
						var myRegMarkOffsetField = measurementEditboxes.add({editValue:(2*2.83465), editUnits:MeasurementUnits.millimeters, smallNudge:0.5});
					}
				}
			}
			with(borderPanels.add()){
				staticTexts.add({staticLabel:"Вокруг:"});
				var myRangeButtons = radiobuttonGroups.add();
				with(myRangeButtons){
					radiobuttonControls.add({staticLabel:"каждого объекта", checkedState:true});
					radiobuttonControls.add({staticLabel:"всего выбранного"});
				}
			}
		}
	}
	var myReturn = myDialog.show();
	if (myReturn == true){
		//Get the values from the dialog box.
		var myDoCropMarks = myCropMarksGroup.checkedState;
		var myDoRegMarks = myRegMarksGroup.checkedState;
		var myDoWhiteBox = myWhiteBox.checkedState;
		var myCropMarkLength = myCropMarkLengthField.editValue* 0.35278;
		var myCropMarkOffset = myCropMarkOffsetField.editValue* 0.35278;
		var myCropMarkBleed = myCropMarkBleedField.editValue* 0.35278;
		var myCropMarkWidth = myCropMarkWidthField.editValue;
		var myRegMarkInnerRadius = myRegMarkInnerRadiusField.editValue* 0.35278;
		var myRegMarkOuterRadius = myRegMarkOuterRadiusField.editValue* 0.35278;
		var myRegMarkOffset = myRegMarkOffsetField.editValue* 0.35278;
		var myRange = myRangeButtons.selectedButton;
		var myCropColor = mySwatchNamesList[myLabelIndex.selectedIndex];
		myDialog.destroy();
		//"||" is logical OR in JavaScript.
		if ((myDoCropMarks != false) || (myDoRegMarks != false)){
			myDrawPrintersMarks(myRange, myDoCropMarks, myDoRegMarks, myCropMarkLength, myCropMarkOffset,
			 myCropMarkBleed, myCropMarkWidth, myRegMarkInnerRadius, myRegMarkOuterRadius, myRegMarkOffset, myDoWhiteBox, myCropColor);
		}
		else{
			alert("Все опции отключены");
		}
	}
	else{
		myDialog.destroy();
	}
}

function deleteUnusedSwatch(){
	var id, sw;
	var myDocument = app.activeDocument;
	while (myDocument.unusedSwatches[0].name != "") {
		id = myDocument.unusedSwatches[0].id;
		sw = myDocument.swatches.itemByID(id);
		sw.remove();
	}
}

function excludeSwatches(colorName) {
	var mySwatchNamesListExclude = new Array('Cyan', 'Magenta', 'Yellow', 'Black', '', 'kroj', 'cut', 'registration', 'paper', 'nog', 'PageBorder');
	var colorExclude = false;
	for (i = 0; i < mySwatchNamesListExclude.length; i++) {
		if (colorName.toLowerCase() == mySwatchNamesListExclude[i].toLowerCase()) {
			colorExclude = true;
		}
	}
	return colorExclude;
}

function myDrawPrintersMarks(myRange, myDoCropMarks, myDoRegMarks, myCropMarkLength, myCropMarkOffset, myCropMarkBleed, 
												myCropMarkWidth, myRegMarkInnerRadius, myRegMarkOuterRadius, myRegMarkOffset, myDoWhiteBox, myCropColor){
	var myBounds, myX1, myY1, myX2, myY2, myObject;
	var myDocument = app.activeDocument;
	var myOldRulerOrigin = myDocument.viewPreferences.rulerOrigin;
	myDocument.viewPreferences.rulerOrigin = RulerOrigin.spreadOrigin;
	//Save the current measurement units.
	var myOldXUnits = myDocument.viewPreferences.horizontalMeasurementUnits;
	var myOldYUnits = myDocument.viewPreferences.verticalMeasurementUnits;
	//Set the measurement units to points.
	myDocument.viewPreferences.horizontalMeasurementUnits = MeasurementUnits.millimeters;
	myDocument.viewPreferences.verticalMeasurementUnits = MeasurementUnits.millimeters;
	//Create a layer to hold the printers marks (if it does not already exist).
	var myLayerName = namingLayer('Cropmarks');
	var myLayer = myDocument.layers.add({name: myLayerName});
	//Get references to the Registration color and the None swatch.
	var myRegistrationColor = myDocument.colors.item("Registration");
	var myNoneSwatch = myDocument.swatches.item("None");
	//Process the objects in the selection.		
	myBounds = myDocument.selection[0].visibleBounds;
	for(var myCounter = 0; myCounter < myDocument.selection.length; myCounter ++){
		myObject = myDocument.selection[myCounter];
		// делаем белую подложку под объект
		if (myDoWhiteBox) {
			myObject.fillColor = myDocument.colors.item("Paper");
		}
		myBounds = myObject.visibleBounds;
		//Set up some initial bounding box values.
		if ((myRange != 0)&&(myCounter==0)){
			myX1 = myBounds[1];
			myY1 = myBounds[0];
			myX2 = myBounds[3];
			myY2 = myBounds[2];
		}
		if(myRange == 0){
			if (myDoCropMarks == true){
				myDrawCropMarks (myBounds[1], myBounds[0], myBounds[3], myBounds[2], myCropMarkLength, myCropMarkOffset, myCropMarkBleed, myCropMarkWidth, myCropColor, myNoneSwatch, myLayer);
			}
			if (myDoRegMarks == true){
				myDrawRegMarks (myBounds[1], myBounds[0], myBounds[3], myBounds[2], myRegMarkOffset, myRegMarkInnerRadius, myRegMarkOuterRadius, myCropMarkWidth,myRegistrationColor, myNoneSwatch, myLayer);
			}
		}
		else{
			//Compare the bounds values to the stored bounds.
			//If a given bounds value is less than (for x1 and y1) or 
			//greater than (for x2 and y2) the stored value,
			//then replace the stored value with the bounds value.
			if (myBounds[0] < myY1){
				myY1 = myBounds[0];
			}
			if (myBounds[1] < myX1){
				myX1 = myBounds[1];
			}
			if (myBounds[2] > myY2){
				myY2 = myBounds[2];
			}
			if (myBounds[3] > myX2){
				myX2 = myBounds[3];
			}
		}
	}
	if(myRange != 0){
		if (myDoCropMarks == true){
			myDrawCropMarks (myX1, myY1, myX2, myY2, myCropMarkLength, myCropMarkOffset, myCropMarkBleed, myCropMarkWidth, myCropColor, myNoneSwatch, myLayer);
		}
		if (myDoRegMarks == true){
			myDrawRegMarks (myX1, myY1, myX2, myY2, myRegMarkOffset, myRegMarkInnerRadius, myRegMarkOuterRadius, myCropMarkWidth, myRegistrationColor, myNoneSwatch, myLayer);
		}
	}
	myDocument.viewPreferences.rulerOrigin = myOldRulerOrigin;
	//Set the measurement units back to their original state.
	myDocument.viewPreferences.horizontalMeasurementUnits = myOldXUnits;
	myDocument.viewPreferences.verticalMeasurementUnits = myOldYUnits;
}

function namingLayer(nameIn, counterL) {
	if (counterL === undefined) {
		counterL = 0;
	}
	counterLayer = counterL;
	var name;
	if (counterLayer == 0) {
		name = nameIn;
	} else {
		name = nameIn + counterLayer;
	}
	for (i = 0; i < app.activeDocument.layers.length; i++) {
		if (app.activeDocument.layers.item(i).name == name) {
			namingLayer(nameIn, ++counterLayer);
			break;
		}
	}
	if (counterLayer == 0) {
		return nameIn;
	} 
	else {
		return nameIn + counterLayer;
	}
}

// параметр myRegistrationColor принимает цвет выбранный из выпадающего меню, по умолчанию - Registration
function myDrawCropMarks (myX1, myY1, myX2, myY2, myCropMarkLength, myCropMarkOffset, myCropMarkBleed, myCropMarkWidth, myRegistrationColor, myNoneSwatch, myLayer){

	//Upper left crop mark pair.
	myDrawLine([myY1+myCropMarkBleed, myX1-myCropMarkOffset, myY1+myCropMarkBleed, myX1-(myCropMarkOffset + myCropMarkLength)], myCropMarkWidth, myRegistrationColor, myNoneSwatch, myLayer);
	myDrawLine([myY1-myCropMarkOffset, myX1+myCropMarkBleed, myY1-(myCropMarkOffset+myCropMarkLength), myX1+myCropMarkBleed], myCropMarkWidth, myRegistrationColor, myNoneSwatch, myLayer);

	//Lower left crop mark pair.
	myDrawLine([myY2-myCropMarkBleed, myX1-myCropMarkOffset, myY2-myCropMarkBleed, myX1-(myCropMarkOffset+myCropMarkLength)], myCropMarkWidth, myRegistrationColor, myNoneSwatch, myLayer);
	myDrawLine([myY2+myCropMarkOffset, myX1+myCropMarkBleed, myY2+myCropMarkOffset+myCropMarkLength, myX1+myCropMarkBleed], myCropMarkWidth, myRegistrationColor, myNoneSwatch, myLayer);

	//Upper right crop mark pair.
	myDrawLine([myY1+myCropMarkBleed, myX2+myCropMarkOffset, myY1+myCropMarkBleed, myX2+myCropMarkOffset+myCropMarkLength], myCropMarkWidth, myRegistrationColor, myNoneSwatch, myLayer);
	myDrawLine([myY1-myCropMarkOffset, myX2-myCropMarkBleed, myY1-(myCropMarkOffset+myCropMarkLength), myX2-myCropMarkBleed], myCropMarkWidth, myRegistrationColor, myNoneSwatch, myLayer);

	//Lower left crop mark pair.
	myDrawLine([myY2-myCropMarkBleed, myX2+myCropMarkOffset, myY2-myCropMarkBleed, myX2+myCropMarkOffset+myCropMarkLength], myCropMarkWidth, myRegistrationColor, myNoneSwatch, myLayer);
	myDrawLine([myY2+myCropMarkOffset, myX2-myCropMarkBleed, myY2+myCropMarkOffset+myCropMarkLength, myX2-myCropMarkBleed], myCropMarkWidth, myRegistrationColor, myNoneSwatch, myLayer);
}

function myDrawRegMarks (myX1, myY1, myX2, myY2, myRegMarkOffset, myRegMarkInnerRadius, myRegMarkOuterRadius, myCropMarkWidth, myRegistrationColor, myNoneSwatch, myLayer){
	var myBounds
	var myXCenter = myX1 + ((myX2 - myX1)/2);
	var myYCenter = myY1 + ((myY2 - myY1)/2);
	var myTargetCenter = myRegMarkOffset+(myRegMarkOuterRadius);

	//Top registration target.
	myBounds = [myY1-(myTargetCenter+myRegMarkInnerRadius), myXCenter-myRegMarkInnerRadius, (myY1-myTargetCenter)+myRegMarkInnerRadius, myXCenter + myRegMarkInnerRadius];
	myDrawTarget(myBounds, myCropMarkWidth, myRegistrationColor, myNoneSwatch, myLayer);
	myBounds = [myY1-(myTargetCenter+myRegMarkOuterRadius), myXCenter, (myY1-myTargetCenter)+myRegMarkOuterRadius, myXCenter]
	myDrawLine(myBounds, myCropMarkWidth, myRegistrationColor, myNoneSwatch, myLayer);
	myBounds = [myY1-myTargetCenter, myXCenter-myRegMarkOuterRadius, myY1-myTargetCenter, myXCenter+myRegMarkOuterRadius]
	myDrawLine(myBounds, myCropMarkWidth, myRegistrationColor, myNoneSwatch, myLayer);

	//Left registration target.
	myBounds = [myYCenter-myRegMarkInnerRadius, myX1-(myTargetCenter+myRegMarkInnerRadius), myYCenter+myRegMarkInnerRadius, (myX1 - myTargetCenter) + myRegMarkInnerRadius];
	myDrawTarget(myBounds, myCropMarkWidth, myRegistrationColor, myNoneSwatch, myLayer);
	myBounds = [myYCenter, myX1-(myTargetCenter+myRegMarkOuterRadius), myYCenter, myX1 -myRegMarkOffset]
	myDrawLine(myBounds, myCropMarkWidth, myRegistrationColor, myNoneSwatch, myLayer);
	myBounds = [myYCenter-myRegMarkOuterRadius, myX1-myTargetCenter, myYCenter+myRegMarkOuterRadius, myX1-myTargetCenter]
	myDrawLine(myBounds, myCropMarkWidth, myRegistrationColor, myNoneSwatch, myLayer);

	//Bottom registration target.
	myBounds = [myY2+(myTargetCenter-myRegMarkInnerRadius), myXCenter-myRegMarkInnerRadius, myY2+ myTargetCenter+myRegMarkInnerRadius, myXCenter + myRegMarkInnerRadius];
	myDrawTarget(myBounds, myCropMarkWidth, myRegistrationColor, myNoneSwatch, myLayer);
	myBounds = [myY2+myRegMarkOffset, myXCenter, myY2+myTargetCenter+myRegMarkOuterRadius, myXCenter]
	myDrawLine(myBounds, myCropMarkWidth, myRegistrationColor, myNoneSwatch, myLayer);
	myBounds = [myY2+myTargetCenter, myXCenter-myRegMarkOuterRadius, myY2 + myTargetCenter, myXCenter+myRegMarkOuterRadius]
	myDrawLine(myBounds, myCropMarkWidth, myRegistrationColor, myNoneSwatch, myLayer);

	//Right registration target.
	myBounds = [myYCenter-myRegMarkInnerRadius, myX2+(myTargetCenter-myRegMarkInnerRadius), myYCenter+myRegMarkInnerRadius, myX2 + myTargetCenter + myRegMarkInnerRadius];
	myDrawTarget(myBounds, myCropMarkWidth, myRegistrationColor, myNoneSwatch, myLayer);
	myBounds = [myYCenter, myX2+myRegMarkOffset, myYCenter, myX2+myTargetCenter+myRegMarkOuterRadius]
	myDrawLine(myBounds, myCropMarkWidth, myRegistrationColor, myNoneSwatch, myLayer);
	myBounds = [myYCenter-myRegMarkOuterRadius, myX2+myTargetCenter, myYCenter+myRegMarkOuterRadius, myX2+myTargetCenter]
	myDrawLine(myBounds, myCropMarkWidth, myRegistrationColor, myNoneSwatch, myLayer);

}
function myDrawLine(myBounds, myStrokeWeight, myRegistrationColor, myNoneSwatch, myLayer){
	app.activeWindow.activeSpread.graphicLines.add(myLayer, undefined, undefined,{strokeWeight:myStrokeWeight, fillColor:myNoneSwatch, strokeColor:myRegistrationColor, geometricBounds:myBounds})
}
function myDrawTarget(myBounds, myStrokeWeight, myRegistrationColor, myNoneSwatch, myLayer){
	app.activeWindow.activeSpread.ovals.add(myLayer, undefined, undefined, {strokeWeight:myStrokeWeight, fillColor:myNoneSwatch, strokeColor:myRegistrationColor, geometricBounds:myBounds})
}