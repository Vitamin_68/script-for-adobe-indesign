﻿// Скрипт работает для пролинкованных файлов *.ai и *.pdf (для *.eps недоступно)
// вызывает для выделенных объектов меню "Object Layer Options..."
// где можно отключить любые слои перед печатью (лак, крой и пр.)
// после печати откатывает все изменения назад.
// Возможно сделать Redo (CTRL+Shift+Z) - вернуть изменения.

var mySelCounter = 0;
var scriptOk = true;

app.doScript(main, ScriptLanguage.javascript, undefined, UndoModes.entireScript, "main");
    // возвращаем слои на место
if (scriptOk) {
    try {
        var myMenuActionUndo = app.menuActions.itemByID(265);
        myMenuActionUndo.invoke();
    }
    catch (e) { 
        alert ("Error Undo");
    }
}

function main() {
    if (app.documents.length != 0){
        if (app.selection.length > 0){
        alert("Отключите ненужные слои, распечатайте макет.\nПосле этого все вернется в исходное состояние.")
            var arrSelection = new Array;
            for (var i = 0; i < app.selection.length; i++) {
                arrSelection.push(app.selection[i]);
            }
        app.selection = null;
        }
        else {
            alert("Ничего не выделено.");
            scriptOk = false;
            return;
        }
    
    // отключаем ненужные слои в линках для каждого выделенного объекта
    mySelCounter = layersOff(arrSelection);

    // включаем рамку листа
    pageBorderVisibleOn();

    // печатаем макет
    printMaket();
    }
    else {
        alert("Нет открытого документа.");
        scriptOk = false;
    }
}

function layersOff(arrSelection) {
    var myMenuActionLayer = app.menuActions.itemByName("Object Layer Options...");
    var myCounter = 0;
    for (var i = 0; i < arrSelection.length; i++) {
        app.selection = arrSelection[i];
        if (myMenuActionLayer.isValid && myMenuActionLayer.enabled) {
            myMenuActionLayer.invoke();
            myCounter++;
        }
        else alert ("Для этого элемента нельзя вызвать Object Layer Options");
    }
    return myCounter;
}

function printMaket() {
    var myMenuActionPrint = app.menuActions.itemByName("Print...");
    myMenuActionPrint.invoke();
}

function pageBorderVisibleOn() {
    var layerNameRegEx = new RegExp("Page ")
    for (var i = 0; i < app.activeDocument.layers.length; i++) {
        if (layerNameRegEx.test(app.activeDocument.layers[i].name)) {
            app.activeDocument.layers[i].visible = true;
        }
    }
}